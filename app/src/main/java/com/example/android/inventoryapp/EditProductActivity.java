package com.example.android.inventoryapp;

import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.LoaderManager;
import android.support.v4.app.NavUtils;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.android.inventoryapp.data.InventoryContract.ProductsEntry;

/**
 * Edit a product's details, or add a new product
 */

public class EditProductActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Cursor> {
    // TAG for log information
    private static final String TAG = "EditProductActivity";
    /**
     * Uri held as a reference to the item being edited
     */
    private Uri uri;
    /**
     * Reference to the Loader
     */
    private int LOADER_REF = 1111;
    // References to the user interface elements
    private EditText txtProductName;
    private EditText txtProductDescription;
    private EditText txtProductPrice;
    private EditText txtProductQuantity;
    private EditText txtSupplierName;
    private EditText txtSupplierEmail;
    private ImageView productImageView;
    Button btnSelectImage;
    /**
     * The item data has changed
     */
    private boolean itemChanged = false;
    /**
     * Reference to the activity when choosing an image
     */
    private static final int GET_IMAGE_ACTIVITY = 7777;
    /**
     * Uri of the product image
     */
    private Uri imageUri;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edit_layout);

        // Assign the user interface elements
        txtProductName = (EditText) findViewById(R.id.editProductName);
        txtProductDescription = (EditText) findViewById(R.id.editProductDescription);
        txtProductPrice = (EditText) findViewById(R.id.editProductPrice);
        txtProductQuantity = (EditText) findViewById(R.id.editProductQuantity);
        txtSupplierName = (EditText) findViewById(R.id.editSupplierName);
        txtSupplierEmail = (EditText) findViewById(R.id.editSupplierEmail);
        productImageView = (ImageView) findViewById(R.id.editor_image);
        btnSelectImage = (Button) findViewById(R.id.editor_selectImage);

        // set listeners to indicate that product data has been changed
        txtProductName.setOnTouchListener(onEntryTouched);
        txtProductDescription.setOnTouchListener(onEntryTouched);
        txtProductPrice.setOnTouchListener(onEntryTouched);
        txtProductQuantity.setOnTouchListener(onEntryTouched);
        txtSupplierName.setOnTouchListener(onEntryTouched);
        txtSupplierEmail.setOnTouchListener(onEntryTouched);
        btnSelectImage.setOnTouchListener(onEntryTouched);

        // When the user clicks this button, open a new activity to select an image
        btnSelectImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.v(TAG, "User wants to select image");
                Intent getImageIntent;
                if (Build.VERSION.SDK_INT < 19) {
                    getImageIntent = new Intent(Intent.ACTION_GET_CONTENT);
                } else {
                    getImageIntent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
                    getImageIntent.addCategory(Intent.CATEGORY_OPENABLE);
                }
                getImageIntent.setType("image/*");
                // Check to be sure there is a suitable activity/application to handle this request
                if (getImageIntent.resolveActivity(getPackageManager()) != null) {
                    Log.d(TAG, "Starting activity to select image");
                    startActivityForResult(getImageIntent, GET_IMAGE_ACTIVITY);
                } else {
                    // no activity exists to handle this action
                    Log.e(TAG, "No activity exists to handle the GET_CONTENT request to select an image");
                    // notify the user
                    Toast.makeText(EditProductActivity.this, "No application exists to select an image", Toast.LENGTH_SHORT).show();
                }
            }
        });

        // get the URI from the calling intent
        // no URI means we are editing a new product
        uri = getIntent().getData();
        if (uri == null) {
            // adding a new product
            this.setTitle(getString(R.string.editor_title_new));
        } else {
            // editing an existing product
            this.setTitle(getString(R.string.editor_title_edit));
            // request the product data from the database
            getSupportLoaderManager().initLoader(LOADER_REF, null, this);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.edit_menu, menu);
        return true;
    }

    /**
     * User clicks an item in the options menu
     *
     * @param item
     * @return
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.edit_action_save) {
            // save the item
            saveProduct();
            // exit the activity
            finish();
            return true;
        }
        if (item.getItemId() == android.R.id.home) {
            // User pressed the "home"
            if (!itemChanged) {
                // Nothing has changed, so accommodate the request
                NavUtils.navigateUpFromSameTask(this);
                return true;
            }
            // data has changed
            // See if the user wants to discard the data
            exitConfirmation();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * When an editor has been touched, assume the data has changed
     */
    public View.OnTouchListener onEntryTouched = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            // set itemChanged to true so the user doesn't leave behind any unsaved data
            itemChanged = true;
            return false;
        }
    };

    /**
     * The Select Image activity has returned
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == GET_IMAGE_ACTIVITY && resultCode == RESULT_OK) {
            // If the result is OK (not cancel or some error) and there is valid data
            if (data != null && data.getData() != null) {
                // get the uri from the selected image
                Uri uri = data.getData();
                try {
                    Log.v(TAG, "User selected image at URI: " + uri.toString());
                    imageUri = uri;
                    // Display the image on the ImageView
                    productImageView.setImageURI(imageUri);
                } catch (Exception e) {
                    Log.e(TAG, "Error getting image: " + e.toString());
                }
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    /**
     * Save the product data in the database
     */
    private void saveProduct() {
        // Make sure there is something to save
        if (TextUtils.isEmpty(txtProductName.getText()) &&
                TextUtils.isEmpty(txtProductDescription.getText()) &&
                TextUtils.isEmpty(txtProductPrice.getText()) &&
                TextUtils.isEmpty(txtProductQuantity.getText()) &&
                TextUtils.isEmpty(txtSupplierName.getText()) &&
                TextUtils.isEmpty(txtSupplierEmail.getText())) {
            Toast.makeText(this, getString(R.string.editor_save_nothing), Toast.LENGTH_SHORT).show();
            return;
        }

        // Validate the entries

        String productName = txtProductName.getText().toString();
        // product name cannot be null
        if (productName.isEmpty()) {
            Toast.makeText(this, getString(R.string.editor_save_error_name), Toast.LENGTH_SHORT).show();
            return;
        }
        String productDescription = txtProductDescription.getText().toString();
        // product price must be numeric and greater than zero
        double productPrice = -1;
        try {
            String priceString = txtProductPrice.getText().toString().replace("$", "");
            productPrice = Double.parseDouble(priceString);
            if (productPrice < 0) {
                Toast.makeText(this, R.string.editor_save_error_price, Toast.LENGTH_SHORT).show();
                return;
            }
        } catch (Exception e) {
            Toast.makeText(this, R.string.editor_save_error_priceinvalid, Toast.LENGTH_SHORT).show();
            return;
        }
        // product quantity must be numeric and greater than zero
        int productQuantity;
        try {
            String quantityString = txtProductQuantity.getText().toString();
            productQuantity = Integer.parseInt(quantityString);
            if (productQuantity < 0) {
                Toast.makeText(this, getString(R.string.editor_save_error_quantity), Toast.LENGTH_SHORT).show();
                return;
            }
        } catch (Exception e) {
            Toast.makeText(this, R.string.editor_save_error_quantityinvalid, Toast.LENGTH_SHORT).show();
            return;
        }

        String supplierName = txtSupplierName.getText().toString();
        // supplier name cannot be null
        if (supplierName.isEmpty()) {
            Toast.makeText(this, R.string.editor_save_error_supplier, Toast.LENGTH_SHORT).show();
            return;
        }
        String supplierEmail = txtSupplierEmail.getText().toString();
        // supplier email cannot be null
        if (supplierEmail.isEmpty()) {
            Toast.makeText(this, R.string.editor_save_error_supplieremail, Toast.LENGTH_SHORT).show();
            return;
        }

        // get the image uri
        String imageUriString = null;
        if (imageUri != null) {
            imageUriString = imageUri.toString();
        }

        // put the data into a content values object to store
        ContentValues values = new ContentValues();
        values.put(ProductsEntry.COLUMN_NAME, productName);
        values.put(ProductsEntry.COLUMN_DESCRIPTION, productDescription);
        values.put(ProductsEntry.COLUMN_PRICE, productPrice);
        values.put(ProductsEntry.COLUMN_QUANTITY, productQuantity);
        values.put(ProductsEntry.COLUMN_SUPPLIER_NAME, supplierName);
        values.put(ProductsEntry.COLUMN_SUPPLIER_EMAIL, supplierEmail);
        values.put(ProductsEntry.COLUMN_IMAGE_URI, imageUriString);

        // Insert a new item or update an existing one based on how the activity was called
        if (uri == null) {
            // save a new item
            Uri newProductUri = getContentResolver().insert(ProductsEntry.CONTENT_URI, values);
            if (newProductUri == null) {
                Toast.makeText(this, getString(R.string.save_update_fail), Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(this, getString(R.string.save_new_success), Toast.LENGTH_SHORT).show();
                itemChanged = false;
            }
        } else {
            // update an existing one
            int numEntriesUpdated = getContentResolver().update(uri, values, null, null);
            if (numEntriesUpdated >= 1) {
                Toast.makeText(this, getString(R.string.save_update_success), Toast.LENGTH_SHORT).show();
                itemChanged = false;
            } else {
                Toast.makeText(this, getString(R.string.save_update_fail), Toast.LENGTH_SHORT).show();
            }
        }
    }

    /**
     * User pressed the back button
     */
    @Override
    public void onBackPressed() {
        // if the data hasn't changed, proceed with the action
        if (!itemChanged) {
            super.onBackPressed();
        }
        // There are unsaved changes, see if the user wants to discard them
        exitConfirmation();
    }

    /**
     * Ask the user if they want to discard unsaved changes
     */
    private void exitConfirmation() {
        Log.d(TAG, "Confirming with user if they want to discard changes");
        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(this);
        alertBuilder.setMessage(getString(R.string.exit_confirmation_message));
        alertBuilder.setPositiveButton(getString(R.string.exit_confirmation_positive), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // discard changes and exit
                Log.d(TAG, "User wants to discard changes");
                finish();
            }
        });
        alertBuilder.setNegativeButton(getString(R.string.exit_confirmation_negative), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Log.v(TAG, "User wants to keep editing");
                // dismiss the dialog
                dialog.dismiss();
            }
        });
        AlertDialog alertDialog = alertBuilder.create();
        // show the dialog
        alertDialog.show();
    }

    /**
     * Create a loader with the desired database columns
     */
    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        if (id == LOADER_REF) {
            String[] columns = new String[]{
                    ProductsEntry.COLUMN_ID,
                    ProductsEntry.COLUMN_NAME,
                    ProductsEntry.COLUMN_PRICE,
                    ProductsEntry.COLUMN_DESCRIPTION,
                    ProductsEntry.COLUMN_IMAGE_URI,
                    ProductsEntry.COLUMN_QUANTITY,
                    ProductsEntry.COLUMN_SUPPLIER_NAME,
                    ProductsEntry.COLUMN_SUPPLIER_EMAIL};
            return new CursorLoader(this, uri, columns, null, null, null);
        }
        return null;
    }

    /**
     * Loader has returned some data
     */
    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        // move cursor to the first item (to ensure there is data)
        if (data.moveToFirst()) {
            // get the data
            String name = data.getString(data.getColumnIndex(ProductsEntry.COLUMN_NAME));
            String description = data.getString(data.getColumnIndex(ProductsEntry.COLUMN_DESCRIPTION));
            double price = data.getDouble(data.getColumnIndex(ProductsEntry.COLUMN_PRICE));
            int quantity = data.getInt(data.getColumnIndex(ProductsEntry.COLUMN_QUANTITY));
            String supplierName = data.getString(data.getColumnIndex(ProductsEntry.COLUMN_SUPPLIER_NAME));
            String supplierEmail = data.getString(data.getColumnIndex(ProductsEntry.COLUMN_SUPPLIER_EMAIL));
            String imageUriString = data.getString(data.getColumnIndex(ProductsEntry.COLUMN_IMAGE_URI));
            if (imageUriString != null) {
                imageUri = Uri.parse(imageUriString);
                productImageView.setImageURI(imageUri);
            }

            // set the display elements
            txtProductName.setText(name);
            txtProductDescription.setText(description);
            txtProductPrice.setText(String.format("$ %.2f", price));
            txtProductQuantity.setText(String.valueOf(quantity));
            txtSupplierName.setText(supplierName);
            txtSupplierEmail.setText(supplierEmail);

        } else {
            Log.e(TAG, "Cursor returned no data");
        }

    }

    /**
     * Loader reset. clear data
     */
    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        txtProductName.setText("");
        txtProductDescription.setText("");
        txtProductPrice.setText("");
        txtProductQuantity.setText("");
        txtSupplierName.setText("");
        txtSupplierEmail.setText("");
        productImageView.setImageBitmap(null);
    }
}
