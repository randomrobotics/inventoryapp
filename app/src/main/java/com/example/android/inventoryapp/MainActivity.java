package com.example.android.inventoryapp;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.Toast;

import com.example.android.inventoryapp.data.InventoryContract.ProductsEntry;
import com.example.android.inventoryapp.data.InventoryCursorAdapter;

public class MainActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Cursor> {
    // TAG for log information
    private static final String TAG = "Main Activity";
    /**
     * Custom cursor adapter to display information in the listview
     */
    private InventoryCursorAdapter adapter;
    /**
     * Reference number for the loader
     */
    private static final int LOADER_REFERENCE = 1234;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_layout);
        // The Floating Action Button is used to create a new product
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.btn_main_new_product);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent newProductIntent = new Intent(getApplicationContext(), EditProductActivity.class);
                startActivity(newProductIntent);
            }
        });
        // create the cursor adapter
        adapter = new InventoryCursorAdapter(this, null, 0);
        // get the list view
        ListView productListView = (ListView) findViewById(R.id.list);
        // get and set the empty view (when no products in the database
        View emptyView = findViewById(R.id.empty_view);
        productListView.setEmptyView(emptyView);
        // set the cursor adapter
        productListView.setAdapter(adapter);

        // Initialize the content loader
        getSupportLoaderManager().initLoader(LOADER_REFERENCE, null, this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // display the menu
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    /**
     * When a user clicks on an item in the options menu
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case (R.id.main_menu_insert_dummy_data):
                // Generate dummy data and insert it into the database
                // this happens on a background thread because it takes a few seconds
                Thread DummyDataThread = new Thread(CreateDummyDataTask);
                DummyDataThread.start();
                Toast.makeText(this, "Generating data for database table", Toast.LENGTH_SHORT).show();
                return true;
            case (R.id.main_menu_delete_all):
                // show the confirmation dialog before deleting all database entries
                showDeleteConfirmationDialog();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Confirm the user wants to delete all products in the database
     */
    private void showDeleteConfirmationDialog() {
        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(this);
        alertBuilder.setMessage(getString(R.string.delete_all_conf_message));
        alertBuilder.setPositiveButton(getString(R.string.delete_all_conf_positive), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // delete all the products
                deleteAllProducts();
            }
        });
        alertBuilder.setNegativeButton(getString(R.string.delete_all_conf_negative), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // do not delete anything, simply dismiss the dialog
                dialog.dismiss();
            }
        });
        AlertDialog alertDialog = alertBuilder.create();
        alertDialog.show();
    }

    /**
     * Delete all products in the database and display a message with the result
     */
    private void deleteAllProducts() {
        Log.d(TAG, "Deleting all products in the database");
        int numRowsDeleted = getContentResolver().delete(ProductsEntry.CONTENT_URI, null, null);
        if (numRowsDeleted > 0) {
            Toast.makeText(this, getString(R.string.delete_all_success), Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, getString(R.string.delete_all_fail), Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Create some data in the table
     */
    Runnable CreateDummyDataTask = new Runnable() {
        @Override
        public void run() {
            CreateDummyData();
        }
    };

    private void CreateDummyData() {
        ContentValues values = new ContentValues();
        // create some products from Cyberdyne Systems
        values.put(ProductsEntry.COLUMN_NAME, getString(R.string.t70_name));
        values.put(ProductsEntry.COLUMN_PRICE, 60500);
        values.put(ProductsEntry.COLUMN_DESCRIPTION, getString(R.string.t70_desc));
        values.put(ProductsEntry.COLUMN_IMAGE_URI, getUriStringForDrawable(R.drawable.cyberdyne_systems_t70));
        values.put(ProductsEntry.COLUMN_QUANTITY, 4);
        values.put(ProductsEntry.COLUMN_SUPPLIER_NAME, getString(R.string.supplier3_name));
        values.put(ProductsEntry.COLUMN_SUPPLIER_EMAIL, getString(R.string.supplier3_email));
        getContentResolver().insert(ProductsEntry.CONTENT_URI, values);
        values.clear();

        values.put(ProductsEntry.COLUMN_NAME, getString(R.string.t800_name));
        values.put(ProductsEntry.COLUMN_PRICE, 450000);
        values.put(ProductsEntry.COLUMN_DESCRIPTION, getString(R.string.t800_desc));
        values.put(ProductsEntry.COLUMN_IMAGE_URI, getUriStringForDrawable(R.drawable.cyberdyne_systems_t800));
        values.put(ProductsEntry.COLUMN_QUANTITY, 1);
        values.put(ProductsEntry.COLUMN_SUPPLIER_NAME, getString(R.string.supplier3_name));
        values.put(ProductsEntry.COLUMN_SUPPLIER_EMAIL, getString(R.string.supplier3_email));
        getContentResolver().insert(ProductsEntry.CONTENT_URI, values);
        values.clear();

        // create some products from Aperture Science
        values.put(ProductsEntry.COLUMN_NAME, getString(R.string.portal_name));
        values.put(ProductsEntry.COLUMN_PRICE, 2499.99);
        values.put(ProductsEntry.COLUMN_DESCRIPTION, getString(R.string.portal_desc));
        values.put(ProductsEntry.COLUMN_IMAGE_URI, getUriStringForDrawable(R.drawable.aperture_science_handheld_portal_device));
        values.put(ProductsEntry.COLUMN_QUANTITY, 1);
        values.put(ProductsEntry.COLUMN_SUPPLIER_NAME, getString(R.string.supplier2_name));
        values.put(ProductsEntry.COLUMN_SUPPLIER_EMAIL, getString(R.string.supplier2_email));
        getContentResolver().insert(ProductsEntry.CONTENT_URI, values);
        values.clear();

        values.put(ProductsEntry.COLUMN_NAME, getString(R.string.scube_name));
        values.put(ProductsEntry.COLUMN_PRICE, 115);
        values.put(ProductsEntry.COLUMN_DESCRIPTION, getString(R.string.scube_desc));
        values.put(ProductsEntry.COLUMN_IMAGE_URI, getUriStringForDrawable(R.drawable.aperture_science_weighted_storage_cube));
        values.put(ProductsEntry.COLUMN_QUANTITY, 80);
        values.put(ProductsEntry.COLUMN_SUPPLIER_NAME, getString(R.string.supplier2_name));
        values.put(ProductsEntry.COLUMN_SUPPLIER_EMAIL, getString(R.string.supplier2_email));
        getContentResolver().insert(ProductsEntry.CONTENT_URI, values);
        values.clear();

        values.put(ProductsEntry.COLUMN_NAME, getString(R.string.ccube_name));
        values.put(ProductsEntry.COLUMN_PRICE, 116);
        values.put(ProductsEntry.COLUMN_DESCRIPTION, getString(R.string.ccube_desc));
        values.put(ProductsEntry.COLUMN_IMAGE_URI, getUriStringForDrawable(R.drawable.aperture_science_weighted_companion_cube));
        values.put(ProductsEntry.COLUMN_QUANTITY, 1);
        values.put(ProductsEntry.COLUMN_SUPPLIER_NAME, getString(R.string.supplier2_name));
        values.put(ProductsEntry.COLUMN_SUPPLIER_EMAIL, getString(R.string.supplier2_email));
        getContentResolver().insert(ProductsEntry.CONTENT_URI, values);
        values.clear();

        values.put(ProductsEntry.COLUMN_NAME, getString(R.string.turret_name));
        values.put(ProductsEntry.COLUMN_PRICE, 11999.99);
        values.put(ProductsEntry.COLUMN_DESCRIPTION, getString(R.string.turret_desc));
        values.put(ProductsEntry.COLUMN_IMAGE_URI, getUriStringForDrawable(R.drawable.aperture_science_sentry_turret));
        values.put(ProductsEntry.COLUMN_QUANTITY, 12);
        values.put(ProductsEntry.COLUMN_SUPPLIER_NAME, getString(R.string.supplier2_name));
        values.put(ProductsEntry.COLUMN_SUPPLIER_EMAIL, getString(R.string.supplier2_email));
        getContentResolver().insert(ProductsEntry.CONTENT_URI, values);
        values.clear();

        // Create some products from Acme
        values.put(ProductsEntry.COLUMN_NAME, getString(R.string.anvil_name));
        values.put(ProductsEntry.COLUMN_PRICE, 17);
        values.put(ProductsEntry.COLUMN_DESCRIPTION, getString(R.string.anvil_desc));
        values.put(ProductsEntry.COLUMN_IMAGE_URI, getUriStringForDrawable(R.drawable.acme_anvil));
        values.put(ProductsEntry.COLUMN_QUANTITY, 4);
        values.put(ProductsEntry.COLUMN_SUPPLIER_NAME, getString(R.string.supplier1_name));
        values.put(ProductsEntry.COLUMN_SUPPLIER_EMAIL, getString(R.string.supplier1_email));
        getContentResolver().insert(ProductsEntry.CONTENT_URI, values);
        values.clear();

        values.put(ProductsEntry.COLUMN_NAME, getString(R.string.skates_name));
        values.put(ProductsEntry.COLUMN_PRICE, 1200);
        values.put(ProductsEntry.COLUMN_DESCRIPTION, getString(R.string.skates_desc));
        values.put(ProductsEntry.COLUMN_IMAGE_URI, getUriStringForDrawable(R.drawable.acme_rocket_powered_roller_skates));
        values.put(ProductsEntry.COLUMN_QUANTITY, 2);
        values.put(ProductsEntry.COLUMN_SUPPLIER_NAME, getString(R.string.supplier1_name));
        values.put(ProductsEntry.COLUMN_SUPPLIER_EMAIL, getString(R.string.supplier1_email));
        getContentResolver().insert(ProductsEntry.CONTENT_URI, values);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        if (id == LOADER_REFERENCE) {
            // create a new cursor loader with the columns for display on the Main Activity
            Uri uri = ProductsEntry.CONTENT_URI;
            String[] columns = new String[]{
                    ProductsEntry.COLUMN_ID,
                    ProductsEntry.COLUMN_NAME,
                    ProductsEntry.COLUMN_PRICE,
                    ProductsEntry.COLUMN_DESCRIPTION,
                    ProductsEntry.COLUMN_QUANTITY};
            return new CursorLoader(this,
                    uri, columns,
                    null, null, null);
        }
        return null;
    }

    /**
     * Loader has returned cursor data. display it
     */
    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        adapter.swapCursor(data);
    }

    /**
     * Loader resets data. clear
     *
     * @param loader
     */
    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        if (adapter != null) {
            adapter.swapCursor(null);
        }
    }

    /**
     * Get URI for the given drawable resource, then convert it to a String
     *
     * @param drawable resource
     * @return String
     */
    private String getUriStringForDrawable(int drawable) {
        return getUriForDrawable(drawable).toString();
    }

    /**
     * Get URI for the given drawable resource
     *
     * @param drawable resource
     * @return Uri
     */
    private Uri getUriForDrawable(int drawable) {
        return Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://"
                + getResources().getResourcePackageName(drawable) + '/'
                + getResources().getResourceTypeName(drawable) + '/'
                + getResources().getResourceEntryName(drawable));
    }

}
