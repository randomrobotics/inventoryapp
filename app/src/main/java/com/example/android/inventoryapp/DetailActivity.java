package com.example.android.inventoryapp;

import android.Manifest;
import android.app.Dialog;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.android.inventoryapp.data.InventoryContract.ProductsEntry;


/**
 * Show detailed product information
 */
public class DetailActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Cursor> {
    // TAG for log information
    private static final String TAG = "DetailActivity";
    private Uri uri;
    long id;
    // References to the display elements
    TextView productNameText;
    TextView productDescText;
    TextView productQtyText;
    TextView supplierNameText;
    ImageView productImageView;

    // variables to store product information that is used in multiple places
    String productName;
    int productQuantity;
    String supplierName;
    String supplierEmail;

    /**
     * Reference to the content loader
     */
    private int DETAIL_LOADER = 4567;

    /**
     * Have to request MANAGE_DOCUMENTS permission at runtime apparently
     */
    private int REQUEST_PERMISSION_CODE = 999;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // load the layout
        setContentView(R.layout.detail_layout);

        // get product information from the intent
        uri = getIntent().getData();
        id = ContentUris.parseId(uri);

        // set the references to the display elements
        productNameText = (TextView) findViewById(R.id.detail_name_text);
        productDescText = (TextView) findViewById(R.id.detail_description);
        productQtyText = (TextView) findViewById(R.id.detail_quantity);
        supplierNameText = (TextView) findViewById(R.id.detail_supplier_name);
        productImageView = (ImageView) findViewById(R.id.detail_image);

        // set listeners for the button to place an order with the supplier
        Button btnOrder = (Button) findViewById(R.id.btn_detail_order);
        btnOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PlaceOrder();
            }
        });

        // set listeners for the -1 and +1 buttons
        Button minusOne = (Button) findViewById(R.id.btn_detail_minusone);
        Button plusOne = (Button) findViewById(R.id.btn_detail_addone);
        minusOne.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AdjustQuantity(-1);
            }
        });
        plusOne.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AdjustQuantity(1);
            }
        });

        // set listener for the button to add/subtract a custom amount
        Button addAmount = (Button) findViewById(R.id.btn_detail_custom_add);
        addAmount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ShowCustomQuantityPicker(1);
            }
        });

        Button subtractAmount = (Button) findViewById(R.id.btn_detail_custom_sub);
        subtractAmount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ShowCustomQuantityPicker(-1);
            }
        });

        // Initialize the content loader
        getSupportLoaderManager().initLoader(DETAIL_LOADER, null, this);
    }


    /**
     * Adjust the product quantity by a given amount, checking to see that it doesn't go below zero
     *
     * @param byVal the amount (+ or -) to adjust the quantity
     */
    private void AdjustQuantity(int byVal) {
        int newQty = productQuantity + byVal;
        if (newQty < 0) {
            Log.e(TAG, "User tried to reduce quantity below zero");
            Toast.makeText(this, getString(R.string.qty_cannot_go_negative), Toast.LENGTH_SHORT).show();
            return;
        }
        // Set the new value in a ContentValues and update the database
        ContentValues values = new ContentValues();
        values.put(ProductsEntry.COLUMN_QUANTITY, newQty);
        getContentResolver().update(uri, values, null, null);
    }

    /**
     * Display a dialog to let the user select a custom value to add/remove items
     *
     * @param multiplier +1 to Add items, -1 to Subtract items
     */
    private void ShowCustomQuantityPicker(final int multiplier) {
        final Dialog pickerDialog = new Dialog(this);
        pickerDialog.setContentView(R.layout.number_picker);
        // Set the message for adding or removing products
        String addOrRemove = multiplier > 0 ? getString(R.string.detail_picker_add) : getString(R.string.detail_picker_remove);
        TextView txtInstructions = (TextView) pickerDialog.findViewById(R.id.txtInstructions);
        txtInstructions.setText(String.format(getString(R.string.detail_picker_instructions_placeholder), addOrRemove));
        final EditText txtQty = (EditText) pickerDialog.findViewById(R.id.txtCustomQuantity);
        Button btnOk = (Button) pickerDialog.findViewById(R.id.picker_btnOK);
        Button btnCancel = (Button) pickerDialog.findViewById(R.id.picker_btnCancel);
        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(txtQty.getText())) {
                    Log.d(TAG, "Add/remove custom quantity - no quantity entered");
                    pickerDialog.dismiss();
                    return;
                }
                int val = Integer.parseInt(txtQty.getText().toString());
                if (val < 0) {
                    Toast.makeText(DetailActivity.this, R.string.detail_picker_negative_error, Toast.LENGTH_SHORT).show();
                    return;
                }
                int adjustment = val * multiplier;
                Log.d(TAG, "User wants to adjust quantity by " + String.valueOf(adjustment));
                AdjustQuantity(adjustment);
                pickerDialog.dismiss();
            }
        });
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pickerDialog.dismiss();
            }
        });

        pickerDialog.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the detail_layout menu
        getMenuInflater().inflate(R.menu.detail_menu, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.detail_menu_delete) {
            // delete the product
            deleteProduct();
        }
        if (item.getItemId() == R.id.detail_menu_edit) {
            // open the EditProductActivity with this product as the URI
            Intent editIntent = new Intent(this, EditProductActivity.class);
            editIntent.setData(uri);
            startActivity(editIntent);
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Confirm that the user wants to delete the product
     * If so, delete it
     */
    private void deleteProduct() {
        // show confirmation dialog
        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(this);
        alertBuilder.setMessage(getString(R.string.delete_conf_message));
        alertBuilder.setPositiveButton(getString(R.string.delete_positive), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // Delete the item
                delete();
            }
        });
        alertBuilder.setNegativeButton(getString(R.string.delete_negative), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // just close the dialog
                dialog.dismiss();
            }
        });
        AlertDialog alertDialog = alertBuilder.create();
        alertDialog.show();
    }

    /**
     * Delete the item from the database
     */
    private void delete() {
        Log.d(TAG, "User requested to delete item at uri: " + uri.toString());
        int numDeleted = getContentResolver().delete(uri, null, null);
        if (numDeleted > 0) {
            // item was successfully deleted
            Log.v(TAG, "Item deleted");
            Toast.makeText(this, R.string.item_delete_success, Toast.LENGTH_SHORT).show();
            finish();
        } else {
            // delete action failed
            Log.d(TAG, "Error deleting item");
            Toast.makeText(this, R.string.item_delete_error, Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Place an order with the supplier
     */
    private void PlaceOrder() {
        // Create an intent to send the email
        Intent emailIntent = new Intent(Intent.ACTION_SENDTO);
        emailIntent.setData(Uri.parse("mailto:"));
        // set the subject line and the recipient email address
        emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{supplierEmail});
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Order: " + productName);
        // set some rudimentary email body text
        StringBuilder emailText = new StringBuilder();
        emailText.append(getString(R.string.order_email_text)).append("\r\n");
        emailText.append(productName).append("\r\n");
        emailIntent.putExtra(Intent.EXTRA_TEXT, emailText.toString());
        // check to see if the user has an application that can handle this (email)
        boolean handlerExists = emailIntent.resolveActivity(getPackageManager()) != null;
        if (handlerExists) {
            Log.i(TAG, "Starting application to send order email to supplier");
            // start the activity to send the email
            startActivity(emailIntent);
        } else {
            Log.e(TAG, "No application exists to send an email");
            // notify the user
            Toast.makeText(this, "No email app exists to handle this request", Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Create the content loader with the desired table columns
     */
    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        if (id == DETAIL_LOADER) {
            String[] columns = new String[]{
                    ProductsEntry.COLUMN_ID,
                    ProductsEntry.COLUMN_NAME,
                    ProductsEntry.COLUMN_DESCRIPTION,
                    ProductsEntry.COLUMN_IMAGE_URI,
                    ProductsEntry.COLUMN_QUANTITY,
                    ProductsEntry.COLUMN_SUPPLIER_NAME,
                    ProductsEntry.COLUMN_SUPPLIER_EMAIL};
            return new CursorLoader(this, uri, columns, null, null, null);
        } else {
            return null;
        }
    }

    /**
     * Content loader has returned with data
     */
    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        // move cursor to the first item (to be sure there is a result)
        if (data.moveToFirst()) {
            // get the data
            productName = data.getString(data.getColumnIndex(ProductsEntry.COLUMN_NAME));
            String productDesc = data.getString(data.getColumnIndex(ProductsEntry.COLUMN_DESCRIPTION));
            // get the image uri
            String image_uri_string = data.getString(data.getColumnIndex(ProductsEntry.COLUMN_IMAGE_URI));
            if (image_uri_string != null) {
                Uri imageUri = Uri.parse(image_uri_string);
                // there seems to be a permissions problem right here
                CheckPermissions();
                productImageView.setImageURI(imageUri);
            }

            productQuantity = data.getInt(data.getColumnIndex(ProductsEntry.COLUMN_QUANTITY));
            String qtyString = String.valueOf(productQuantity);
            supplierName = data.getString(data.getColumnIndex(ProductsEntry.COLUMN_SUPPLIER_NAME));
            supplierEmail = data.getString(data.getColumnIndex(ProductsEntry.COLUMN_SUPPLIER_EMAIL));

            // set the displays
            productNameText.setText(productName);
            productDescText.setText(productDesc);
            productQtyText.setText(qtyString);
            supplierNameText.setText(supplierName);

        } else {
            Log.e(TAG, "Loader returned no data");
        }
    }

    /**
     * See if the app needs the permission and request it
     */
    private void CheckPermissions() {
        if (Build.VERSION.SDK_INT > 19) {
            int manage_permission = ContextCompat.checkSelfPermission(this, Manifest.permission.MANAGE_DOCUMENTS);
            if (manage_permission != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.MANAGE_DOCUMENTS)) {
                    AlertDialog.Builder alertBuilder = new AlertDialog.Builder(this)
                            .setTitle("Grant Permission")
                            .setMessage("This app needs the MANAGE_DOCUMENTS permission to function properly")
                            .setPositiveButton("YES", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    requestPermission(Manifest.permission.MANAGE_DOCUMENTS);
                                }
                            });
                    alertBuilder.create().show();
                }
            }
        }
    }

    /**
     * Set the request permission dialog
     */
    private void requestPermission(String permission) {
        ActivityCompat.requestPermissions(this, new String[]{permission}, REQUEST_PERMISSION_CODE);
    }

    /**
     * Get the response from the permission request
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == REQUEST_PERMISSION_CODE) {
            if (grantResults.length > 0) {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.i(TAG, "Permission to MANAGE_DOCUMENTS granted. Yay, the app will actually work");
                } else {
                    Log.e(TAG, "Permission to MANAGE_DOCUMENTS denied. We cannot do anything with images");
                }
            }
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        // loader reset. clear the display data
        productNameText.setText("");
        productDescText.setText("");
        productQtyText.setText("");
        supplierNameText.setText("");
        productImageView.setImageBitmap(null);
    }
}
